<?php
    /* ValfoPHP - Valida��o de Formularios em PHP
     * 
     * --   Manual da classe --
     * 
     *  Para usar a classe de validacao primeiro devemos estanciar a ela.
     *  $validacao  = new Validacao();
     * 
     * 
     *  Ap�s feito isso � necess�rio definir as configura��es de valida��o, isso inclui passar
     *  os dados que devem ser validados e m�todos da classe a serem utilizados.
     *  para os dados no exemplo estarei passando a global $_POST que no caso vem 
     *  com as informacoes do formul�rio, mas pode ser qualquer array de dados, lembrando que
     *  os indices desse array de dados deve ser como as colunas do banco de dados para o mesmo validar por exemplo se j� possui registro equivalente.
     * 
     *  J� nos m�todos que ser�o utilizados poder� passar um array com os indices dos dados
     *  como no exemplo � usado para campos obrigat�rios
     * 
     *  Exemplo de dados, pode ser seu $_POST ou qualquer array.
     *  $dados = array(
     *      'nome'=>'fernando binasco',
     *      'email'=>'fernando.binasco@gmail.com',
     *      'teste'=>''
     *  );
     * 
     *  Exemplo 1 ( obrigatorio nome )
     *  $validacao->_config = array(
     *      'dados'         =>  $dados,
     *      'obrigatorio'   =>  array('nome','email'),
     *      'email'         =>  array('email')
     *  );
     * 
     * Exemplo 2 ( obrigatorio todos os indices )
     * $validacao->_config = array(
     *      'dados'         =>  $dados,
     *      'obrigatorio'   =>  array_keys($dados),
     *      'email'         =>  array('email')
     *  );
     * 
     *  Depois de definido as configura��es basta executar
     *  Nesse exemplo gera um json com a mensagem de erro
     *  if ( is_array( $erro = $validacao->run() ) ) {
     *      $resposta = array('erro'=>1,'mensagem'=>$erro['mensagem']);
     *      print json_encode($resposta);
     *      exit;
     *  }
     * 
     * 
     * --   Fun��es da classe --
     * 
     * @ obrigatorio:   Permite apenas preenchidos
     * @ email:         Verifica se e-mail � valido
     * @ moeda:      Permite apenas numeros
     * @ real           Permite apenas numeros, pontos e virgulas
     * @ cpf            Verifica se CPF � v�lido
     * @ cnpj           Verifica se CNPJ � v�lido
     * @ cpfCnpj        Verifica se CNPJ ou CPF � v�lido
     * 
     * 
     * --   Array de retorno --
     * 
     * @ mensagem:STRING:   Caso de algum problema com a classe retorna mensagem de erro
     * @ erro:BOOLEAN:      Resultado da validacao
     * @ indices:ARRAY:     Array com indices dos dados que nao passaram em alguma validacao
     * 
     * --   Changelog --
     * 
     * 13/12/2012 ( Fernando Binasco )  ->  Adicionado excessao para a classe validar apenas indexes que estejam em dados
     *                                      mesmo que esteja na congif do metodo.
     * 14/12/2012 ( Fernando Binasco )  ->  Adicionado  verificacoes por CPF, CNPJ e CPF ou CNPJ
     * 07/01/2013 ( Fernando Binasco )  ->  Excess�o para quando um campo n�o � obrigat�rio mas precisa ser validado caso 
     *                                      preenchido. Ex: E-mail adicional.
     * 07/01/2013 ( Fernando Binasco )  ->  Adicionado validacao de datas
     */

    class Validacao {
        
        public      $_config;
        private     $retorno;
        
        public function __construct() {
            $this->retorno = null;
        }
        
        /*
         * Executa a validacao metodo a metodo de acordo com a configuracao
         */
        public function run() {
            if ( !is_null( $this->_config ) ) {
                foreach ( $this->_config as $metodo => $indexes ) {
                    if ( $metodo != 'dados' ) {
                        $indexesFiltered = array();
                        foreach ( $indexes as $index ) {
                            # Filtro para saber se a chave do array contem valor em dados
                            if ( in_array( $index, array_keys( $this->_config['dados'] ) ) ) {
                                # Filtro para liberar campos nao preenchidos ( Se chegou aqui � porque passou na chamada dos obrigat�rios.  )
                                if ( !empty( $this->_config['dados'][$index]) || $metodo == 'obrigatorio' ) {
                                    $indexesFiltered[] = $index;
                                }
                            }
                        }
                        if ( is_null( $this->retorno ) ) {
                            $this->$metodo( $indexesFiltered );
                        } else {
                            return $this->retorno;
                        }
                    }
                }
            } else {
                $this->retorno = array (
                    'erro'      => true,
                    'mensagem'  => utf8_encode('Obrigat�rio definir configura��es da classe de valida��o')
                );
            }
            return is_null($this->retorno)?array('erro'=>false):$this->retorno;
        }

        /*
         * Verificacoes:
         * -Valores dos indices dos dados estao preenchidos
         */
        private function obrigatorio( $indexes ) {
            $errorData = array();
            foreach ( $indexes as $index ) {
                if ( $this->_config['dados'][$index] === '') {
                    $errorData[] = $index;
                }
            }
            if ( count($errorData) > 0 ) {
                $this->retorno = array(
                    'erro'      =>  true,
                    'mensagem'  =>  utf8_encode('Preenchimento obrigat�rio'),
                    'indices'   =>  $errorData
                );
            }
        }

         /*
         * Verificacoes:
         * -Formato da escrita do e-mail
         * -Dominio existente ou n�o
         */
        private function email( $indexes ) {
            $errorData = array();
            foreach ( $indexes as $index ) {
                $email = $this->_config['dados'][$index];
                
                # confere formato
                if ( filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
                    #confere dominio
                    list( $usuario, $dominio ) = split ( '@', $email, 2 ); unset( $usuario );
                    if ( !checkdnsrr( $dominio, 'MX' ) && !checkdnsrr( $dominio, 'A' ) )
                        $errorData[] = $index;
                } else {
                    $errorData[] = $index;
                }
            }
            if ( count($errorData) > 0 ) {
                $this->retorno = array(
                    'erro'      => true,
                    'mensagem'  => utf8_encode('E-mail inv�lido'),
                    'indices'   => $errorData
                );
            }
        }

        /*
         * Verificacoes:
         * -Numerico ou nao
         */
        private function numerico( $indexes ) {
            $errorData = array();
            foreach ( $indexes as $index )
                if ( !is_numeric($this->_config['dados'][$index]) )
                    $errorData[] = $index;
            if ( count($errorData) > 0 ) {
                $this->retorno = array(
                    'erro'      => true,
                    'mensagem'  => utf8_encode('Campo(s) devem ser preenchidos somente com numeros'),
                    'indices'   => $errorData
                );
            }
        }
        
        /*
         * Verificacoes:
         * -Numerico aceitando porntos e virgulas
         */
        private function moeda( $indexes ) {
            $errorData = array();
            foreach ( $indexes as $index )
                if ( !is_numeric(str_replace( array('.',','), '', $this->_config['dados'][$index]) ) )
                    $errorData[] = $index;
            if ( count($errorData) > 0 ) {
                $this->retorno = array(
                    'erro'      => true,
                    'mensagem'  => utf8_encode('Campo(s) devem conter somente numeros pontos ou v�rgulas'),
                    'indices'   => $errorData
                );
            }
        }

        /*
         * Verificacoes:
         * - Logica do CPF valida ou nao
         */
        private function cpf( $indexes ) {
            $errorData = array();
            foreach ( $indexes as $index ) {
                $cpf = str_replace( array( '.', '-', '/', ',', ' ' ), '', $this->_config['dados'][$index] );
                if ( empty($cpf) || !is_numeric($cpf) ) {
                    $errorData[] = $index;
                } else {
                    $cpf = @eregi_replace('[^0-9]', '', $cpf);
                    if ( strlen($cpf) != 11 ) {
                         $errorData[] = $index;
                    } else {
                        $dv     = substr( $cpf, -2 );
                        $compdv = 0;
                        $nulos  = array('12345678909');
                        for ($i=0; $i<=9; $i++ )
                            $nulos[] = $i.$i.$i.$i.$i.$i.$i.$i.$i.$i.$i;
                        if ( in_array( $cpf, $nulos ) ) {
                             $errorData[] = $index;
                        } else {
                            $acum = 0;
                            for ( $i=0; $i<9; $i++ )
                                $acum += $cpf[$i]*(10-$i);
                            $x      = $acum % 11;
                            $acum   = ($x>1) ? (11 - $x) : 0;
                            $compdv = $acum * 10;
                            $acum   = 0;
                            for ( $i=0; $i<10; $i++ )
                                $acum += $cpf[$i]*(11-$i);
                            $x      = $acum % 11;
                            $acum   = ($x>1) ? (11 - $x) : 0;
                            $compdv = $compdv + $acum;
                            if ( $compdv != $dv )
                                $errorData[] = $index;
                        }
                    }
                }
            }
            if ( count($errorData) > 0 )
                $this->retorno = array(
                    'erro'      => true,
                    'mensagem'  => utf8_encode('CPF inv�lido'),
                    'indices'   => $errorData
                );
        }

        /*
         * Verificacoes:
         * - Logica do CNPJ valida ou nao
         */
        private function cnpj( $indexes ) {
            $errorData = array();
            foreach ( $indexes as $index ) {
                $Cnpj = str_replace(array('.','-','/',',',' '), '', $this->_config['dados'][$index]);
                if ( !is_numeric($Cnpj) ) {
                    $errorData[] = $index;
                } else {
                    $RecebeCNPJ=${"Cnpj"};
                    $s="";
                    for ( $x=1; $x<=strlen( $RecebeCNPJ ); $x++ ) {
                        $ch = substr( $RecebeCNPJ, $x-1, 1);
                        if ( ord( $ch ) >= 48 && ord ( $ch ) <= 57 ) {
                            $s=$s.$ch;
                        }
                    }
                    $RecebeCNPJ=$s;
                    if ($RecebeCNPJ=="00000000000000") {
                        $then;
                        $errorData[] = $index;
                    } else {
                        for ( $i=1; $i <=14; $i++) {
                            $n[$i]=intval(substr($RecebeCNPJ,$i-1,1));
                        }
                        $soma = $n[1]*5+$n[2]*4+$n[3]*3+$n[4]*2+$n[5]*9+$n[6]*8+$n[7]*7+$n[8]*6+$n[9]*5+$n[10]*4+$n[11]*3+$n[12]*2;
                        $soma=$soma-(11*(intval($soma/11)));
                        if ( $soma==0 || $soma==1 ) {
                            $r1=0;
                        } else {
                            $r1=11-$soma;
                        }
                        if ( $r1 == $n[13] ) {
                            $soma=$n[1]*6+$n[2]*5+$n[3]*4+$n[4]*3+$n[5]*2+$n[6]*9+$n[7]*8+$n[8]*7+$n[9]*6+$n[10]*5+$n[11]*4+$n[12]*3+$n[13]*2;
                            $soma=$soma-(11*(intval($soma/11)));
                            if ($soma==0 || $soma==1) {
                                $r2=0;
                            } else {
                                $r2=11-$soma;
                            }
                            if ($r2==$n[14]) {
                                return true;
                            } else {
                                $errorData[] = $index;
                            }
                        } else {
                            $errorData[] = $index;
                        }
                    }
                }
            }
            if ( count($errorData) > 0 )
                $this->retorno = array(
                    'erro'      => true,
                    'mensagem'  => utf8_encode('CNPJ inv�lido'),
                    'indices'   => $errorData
                );
        }
        
        /*
         * Verificacoes:
         * - Logica do CPF ou CNPJ valida ou nao
         */
        private function cpfCnpj( $indexes ) {
            # executa a validacao como cpf
            $this->retorno = null;
            $this->cpf ( $indexes );
            $respostaCpf = $this->retorno;
            # executa a valida��o como cnpj
            $this->retorno = null;
            $this->cnpj( $indexes );
            $respostaCnpj = $this->retorno;
            if ( !is_null($respostaCpf) && !is_null($respostaCnpj) ) {
                $this->retorno = array(
                    'erro'      => true,
                    'mensagem'  => utf8_encode('CPF / CNPJ inv�lido'),
                    'indices'   => $indexes
                );
            } else {
                $this->retorno = null;
            }
        }
        
        /*
         * Verifica��es
         * - Formato da data
         * - Se � uma data v�lida ou n�o de acordo com o checkdate do PHP
         */
        private function data( $indexes ) {
            $errorData = array();
            foreach ( $indexes as $index ) {
                if ( strlen( $this->_config['dados'][$index] ) == 10 ) {
                    $data = explode( '/', $this->_config['dados'][$index] );
                    if ( !checkdate($data[1], $data[0], $data[2]) ) {
                        $errorData[] = $index;
                    }
                }
            }
            if ( count($errorData) > 0 ) {
                $this->retorno = array(
                    'erro'      => true,
                    'mensagem'  => utf8_encode('Data inv�lida'),
                    'indices'   => $errorData
                );
            }
        }

    }

/*
    $validacao  = new Validacao();
    
    $dados = array(
        'cpf'=>'05046336944',
        'cpfruim'=>'44.981.996/0001-39000',
        'numero'=>'8'
    );

    $validacao->_config = array(
        'dados'         =>  $dados,
        'obrigatorio'   =>  array_keys($dados),
        #'email'         =>  array('email'),
        #'numerico'      =>  array('numero')
         'cpfCnpj'      =>  array('cpf','cpfruim')
        #'cpf'      =>  array('cpf','cpfruim')
    );
 
     var_dump($validacao->run());
    # $validacao->run();
*/
 
?>
